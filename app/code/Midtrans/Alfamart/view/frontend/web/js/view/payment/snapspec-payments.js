define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
        ) {
        'use strict';
        rendererList.push(
            {
                type: 'alfamart',
                component: 'Midtrans_Alfamart/js/view/payment/method-renderer/alfamart-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
